﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void UnloadCurrentScene()
    {
        SceneManager.UnloadSceneAsync(gameObject.scene.name);
    }

    public void OpenTwitter()
    {
        Application.OpenURL("https://twitter.com/");
    }

    public void OpenInsta()
    {
        Application.OpenURL("https://www.instagram.com/");
    }

    public void OpenHP()
    {
        Application.OpenURL("https://caerux.com/");
    }
}
