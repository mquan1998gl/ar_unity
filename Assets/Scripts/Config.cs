﻿public class Config
{
    public static string name_LIST = "2_List";
    public static string name_RECORD = "3_Record";
    public static string name_SPLASH = "1_Splash";
    public static string name_PROFILE = "4_Profile";
    public static string name_TUTORIAL = "5_Tutorial";

    public const int OMEDETO = 1;
    public const int TANYOBI = 2;
    public const int NYUGAKU = 3;
    public const int SOTSUGYO = 4;
    public const int KEKKON = 5;
    public const int ARIGATO = 6;
    public const int SUKI = 7;
    public const int GOMEN = 8;
    public const int NENMATSU = 9;
    public const int SHINNEN = 10;
    public const int HIKKOSHI = 11;


    public const int Ani_OPEN = 1;
    public const int Ani_JUMP = 2;
    public const int Ani_CRY = 3;
    public const int Ani_YORO = 4;
    public const int Ani_HIKKOSHI = 5;
    public const int Ani_SAKURA = 6;
    public const int Ani_HEART = 7;
    public const int Ani_KIRA = 8;
    public const int Ani_HYOKO = 9;
    public const int Ani_OME = 10;
    public const int Ani_ARI = 11;

}
