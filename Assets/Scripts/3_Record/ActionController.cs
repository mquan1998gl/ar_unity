﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using GetSocialSdk.Capture.Scripts;

#if PLATFORM_IOS
using UnityEngine.iOS;
using UnityEngine.Apple.ReplayKit;
#endif

public class ActionController : MonoBehaviour
{
    public ObjectController objController;
    public GameObject shotButton;
    public GameObject exitButton;
    AndroidJavaObject androidRecorder;

    public int zoomSpeed;
    public int roteSpeed;

    float screenSize;
    DateTime start = DateTime.MaxValue;
    DateTime tapStart = DateTime.MinValue;
    
    public bool enableMicrophone = false;
    public bool enableCamera = false;
    bool recording = false;
    bool isFinishing = false;
    bool isPreview = false;

    void Awake()
    {
        screenSize = Vector2.Distance(new Vector2(0, 0), new Vector2(Screen.width, Screen.height));
    }

    private void Start()
    {

#if PLATFORM_IOS
        if (!ReplayKit.APIAvailable)
        {
            shotButton.SetActive(false);
        }
#elif UNITY_ANDROID
        using (AndroidJavaClass unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            androidRecorder = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
            androidRecorder.Call("setUpSaveFolder", "Tee");//custom your save folder to Movies/Tee, by defaut it will use Movies/AndroidUtils
            int width = Screen.width;
            int height = Screen.height;
            int bitrate = (int)(1f * width * height / 100 * 240 * 7);
            int fps = 30;
            bool audioEnable = true;
            androidRecorder.Call("setupVideo", width, height, bitrate, fps, audioEnable);//this line manual sets the video record setting. You ca use the defaut setting by comment this code block
            if (!IsPermitted(AndroidPermission.RECORD_AUDIO))//RECORD_AUDIO is declared inside plugin manifest but we need to request it manualy
            {
                RequestPermission(AndroidPermission.RECORD_AUDIO);
            }
        }
#endif
    }

    // Update is called once per frame
    void Update()
    {

#if PLATFORM_IOS
        if (ReplayKit.recordingAvailable && !isPreview)
        {
            Debug.Log("ReplayKit.recordingAvailable : true");
            ReplayKit.Preview();
            isPreview = true;
        }
#endif
        if (objController.IsARObjectExist())
        {
            if (Input.touchCount > 1) //--------------Pinch in - out-------------
            {
                Touch tZero = Input.GetTouch(0);
                Touch tOne = Input.GetTouch(1);

                Vector2 tZeroPrevious = tZero.position - tZero.deltaPosition;
                Vector2 tOnePrevious = tOne.position - tOne.deltaPosition;

                float oldTouchDistance = Vector2.Distance(tZeroPrevious, tOnePrevious);
                float currentTouchDistance = Vector2.Distance(tZero.position, tOne.position);

                float deltaDistance = oldTouchDistance - currentTouchDistance;
                float deltaZoom = 1 - (deltaDistance / screenSize) * zoomSpeed;
                objController.ChangeScale(deltaZoom);
            }
            else if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Moved)// Detects Swipe while finger is still moving
                {
                    float deltaDistance = (touch.deltaPosition.x / Screen.width) * roteSpeed;
                    float deltaRote = - 90 * deltaDistance;
                    objController.ChangeRotation(deltaRote);
                }
            }
           }
        else
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    start = DateTime.Now;
                }

                //Detects swipe after finger is released
                if (touch.phase == TouchPhase.Ended)
                {
                    double deltaTime = (DateTime.Now - start).TotalMilliseconds;
                    if (deltaTime > 500) return; //long press
                    else // Single tap
                    {
                        objController.AddReferencePoint(touch.position); // Add new AR object
                    }

                }
            }
        }
    }

    public void BackToSelectScene()
    {
        objController.RemoveAllReferencePoints();
        SceneManager.LoadScene(Config.name_LIST);
    }

    public void RecordScreen()
    {
        if (isFinishing) return;
        recording = !recording;
        if (recording)
        {
            StartCoroutine(StartRecord());
        }
        else
        {
            StartCoroutine(StopRecord());
        }
    }

    IEnumerator StartRecord()
    {
        shotButton.GetComponent<CanvasGroup>().alpha = 0f;
        exitButton.GetComponent<CanvasGroup>().alpha = 0f;
        yield return new WaitForSeconds(0.5f);
    #if PLATFORM_IOS
            string lastError = "";
            try
            {
                ReplayKit.StartRecording(enableMicrophone, enableCamera);
            }
            catch (Exception e)
            {
                lastError = e.ToString();
                Debug.LogError("StartRecord failed: " + lastError);
            }
#elif UNITY_ANDROID
        if (!IsPermitted(AndroidPermission.RECORD_AUDIO))//RECORD_AUDIO is declared inside plugin manifest but we need to request it manualy
        {
            RequestPermission(AndroidPermission.RECORD_AUDIO);
        }
        else
        {
            androidRecorder.Call("startRecording");
        }
#endif
        yield return new WaitForSeconds(30f);
        StartCoroutine(StopRecord());
    }

    IEnumerator StopRecord()
    {
        StopCoroutine("StartRecord");
        isFinishing = true;
        isPreview = false;
    #if PLATFORM_IOS
            string lastError = "";
            try
            {
                ReplayKit.StopRecording();
            }
            catch (Exception e)
            {
                lastError = e.ToString();
                Debug.LogError("StopRecord failed: " + lastError);
            }
    #elif UNITY_ANDROID
        androidRecorder.Call("stopRecording");
    #endif
        yield return new WaitForSeconds(0.5f);
        shotButton.GetComponent<CanvasGroup>().alpha = 1f;
        exitButton.GetComponent<CanvasGroup>().alpha = 1f;
        isFinishing = false;
    }

    #if UNITY_ANDROID
    bool IsPermitted(AndroidPermission permission)
    {
        using (var androidUtils = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            return androidUtils.GetStatic<AndroidJavaObject>("currentActivity").Call<bool>("hasPermission", GetPermissionStrr(permission));
        }
    }

    void RequestPermission(AndroidPermission permission)
    {
        using (var androidUtils = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            androidUtils.GetStatic<AndroidJavaObject>("currentActivity").Call("requestPermission", GetPermissionStrr(permission));
        }
    }

    string GetPermissionStrr(AndroidPermission permission)
    {
        return "android.permission." + permission.ToString();
    }

    #endif
}

public enum AndroidPermission
{
    ACCESS_COARSE_LOCATION,
    ACCESS_FINE_LOCATION,
    ADD_VOICEMAIL,
    BODY_SENSORS,
    CALL_PHONE,
    CAMERA,
    GET_ACCOUNTS,
    PROCESS_OUTGOING_CALLS,
    READ_CALENDAR,
    READ_CALL_LOG,
    READ_CONTACTS,
    READ_EXTERNAL_STORAGE,
    READ_PHONE_STATE,
    READ_SMS,
    RECEIVE_MMS,
    RECEIVE_SMS,
    RECEIVE_WAP_PUSH,
    RECORD_AUDIO,
    SEND_SMS,
    USE_SIP,
    WRITE_CALENDAR,
    WRITE_CALL_LOG,
    WRITE_CONTACTS,
    WRITE_EXTERNAL_STORAGE
}