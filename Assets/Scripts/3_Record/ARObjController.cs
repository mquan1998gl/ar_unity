﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARObjController : MonoBehaviour
{
    bool isRunning = false;
    public MotionController motionController;
    public FukidashiController fukidashiController;
    public ObjectController objectController;
    void Update()
    {
        if (isRunning) return;
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Ended))
        {
            Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                StartCoroutine(runAnimation());
            }
        }
    }

    IEnumerator runAnimation()
    {
        isRunning = true;
        int animationId = GetAnimation();
        motionController.SetAnimetion(animationId);
        fukidashiController.ShowMessage(true);
        switch (GameSettingManager.Instance.Selected)
        {
            case Config.OMEDETO:
                objectController.ShowKamifubuki(true);
                break;
            case Config.TANYOBI:
                objectController.ShowKirakira(true);
                break;
            case Config.NYUGAKU:
            case Config.SOTSUGYO:
                objectController.ShowSakura(true);
                break;
            case Config.KEKKON:
                objectController.ShowKirakira(true);
                break;
            case Config.ARIGATO:
                break;
            case Config.SUKI:
                break;
            case Config.GOMEN:
                objectController.ShowCryWater(true);
                break;
            case Config.NENMATSU:
                break;
            case Config.SHINNEN:
                break;
            case Config.HIKKOSHI:
                break;
            default:
                break;
        }
        //GameSettingManager.Instance.playAudio();
        yield return new WaitForSeconds(30);
        motionController.SetAnimetion(0);
        fukidashiController.ShowMessage(false);
        objectController.ShowKirakira(false);
        objectController.ShowCryWater(false);
        objectController.ShowKamifubuki(false);
        objectController.ShowSakura(false);
        //GameSettingManager.Instance.stopAudio();
        isRunning = false;
    }

    private int GetAnimation()
    {
        switch (GameSettingManager.Instance.Selected)
        {
            case Config.OMEDETO:
                return Config.Ani_OME;
            case Config.TANYOBI:
                return Config.Ani_KIRA;
            case Config.NYUGAKU:
                return Config.Ani_SAKURA;
            case Config.SOTSUGYO:
                return Config.Ani_SAKURA;
            case Config.KEKKON:
                return Config.Ani_KIRA;
            case Config.ARIGATO:
                return Config.Ani_ARI;
            case Config.SUKI:
                return Config.Ani_HEART;
            case Config.GOMEN:
                return Config.Ani_CRY;
            case Config.NENMATSU:
                return Config.Ani_OME;
            case Config.SHINNEN:
                return Config.Ani_YORO;
            case Config.HIKKOSHI:
                return Config.Ani_HIKKOSHI;
            default:
                return 0;
        }
    }
}
