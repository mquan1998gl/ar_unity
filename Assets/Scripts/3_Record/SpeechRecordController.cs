﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TextSpeech;

public class SpeechRecordController : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        SpeechToText.instance.Setting("en-US");
    }

    public void StartRecording()
    {
        Debug.Log("StartRecording **** ");
#if UNITY_EDITOR
#else
        SpeechToText.instance.StartRecording("Speak any");
#endif
    }

    public void StopRecording()
    {
        Debug.Log("StopRecording **** ");
#if UNITY_EDITOR
        OnResultSpeech("Not support in editor.");
#else
        SpeechToText.instance.StopRecording();
#endif
//#if UNITY_IOS
//        loading.SetActive(true);
//#endif
    }
    void OnResultSpeech(string _data)
    {
        Debug.Log("OnResultSpeech **** " + _data);
//#if UNITY_IOS
//        loading.SetActive(false);
//#endif
    }
}
