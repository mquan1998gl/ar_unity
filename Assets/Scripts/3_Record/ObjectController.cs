﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ObjectController : MonoBehaviour
{
    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    ARRaycastManager m_RaycastManager;
    ARAnchorManager m_ReferencePointManager;
    List<ARAnchor> m_ReferencePoint;
    ARPlaneManager m_PlaneManager;
    GameObject m_Camera;
    GameObject ARObject;
    public GameObject Water;
    public GameObject Sakura;
    public GameObject Kamifubuki;
    public GameObject Kirakira;
    public GameObject Effect;
    float dis = 1f;

    void Awake()
    {
        m_RaycastManager = GetComponent<ARRaycastManager>();
        m_ReferencePointManager = GetComponent<ARAnchorManager>();
        m_PlaneManager = GetComponent<ARPlaneManager>();
        m_ReferencePoint = new List<ARAnchor>();
        ARObject = null;
    }

    private void Start()
    {
        m_Camera = Camera.main.gameObject;
    }

    private void Update()
    {

        Effect.transform.position = m_Camera.transform.position + m_Camera.transform.forward * dis;
        Effect.transform.rotation = new Quaternion(0.0f, m_Camera.transform.rotation.y, 0.0f, m_Camera.transform.rotation.w);
    }

    public void RemoveAllReferencePoints()
    {
        foreach (var referencePoint in m_ReferencePoint)
        {
            m_ReferencePointManager.RemoveAnchor(referencePoint);
        }
        m_ReferencePoint.Clear();
        EnableARTracking(true);
        ARObject = null;
    }

    public void AddReferencePoint(Vector2 touchPosition)
    {
        if (m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon))
        {
            var hitPose = s_Hits[0].pose;
            TrackableId planeId = s_Hits[0].trackableId;
            var referencePoint = m_ReferencePointManager.AttachAnchor(m_PlaneManager.GetPlane(planeId), hitPose);
            if (referencePoint != null)
            {
                foreach (var point in m_ReferencePoint)
                {
                    m_ReferencePointManager.RemoveAnchor(point);
                }
                m_ReferencePoint.Clear();
                m_ReferencePoint.Add(referencePoint);
                ARObject = referencePoint.gameObject;
                ARObject.transform.Find("Square").Find("Cube").gameObject.GetComponent<ARObjController>().objectController = this;
                ARObject.transform.Find("Square").Find("Fukidashi").gameObject.GetComponent<FukidashiController>().cameraTransform = Camera.main.transform;
                ARObject.transform.Find("Square").GetComponent<MotionController>().SetAnimetion(12);

                EnableARTracking(false);
            }
        }
    }

    private void EnableARTracking(bool isEnable)
    {
        foreach (var plane in m_PlaneManager.trackables)
            plane.gameObject.SetActive(isEnable);
        m_PlaneManager.enabled = isEnable;
    }

    public bool IsARObjectExist()
    {
        return ARObject != null;
    }

    public void ChangeScale(float scale)
    {
        if (ARObject != null) ARObject.transform.localScale *= scale;
    }

    public void ChangeRotation(float rotation)
    {
        if (ARObject != null) ARObject.transform.Rotate(0, rotation, 0);
    }

    public void ShowCryWater(bool Enable)
    {
        Water.SetActive(Enable);
    }

    public void ShowSakura(bool Enable)
    {
        Sakura.SetActive(Enable);
    }

    public void ShowKamifubuki(bool Enable)
    {
        Kamifubuki.SetActive(Enable);
    }

    public void ShowKirakira(bool Enable)
    {
        Kirakira.SetActive(Enable);
    }
}
