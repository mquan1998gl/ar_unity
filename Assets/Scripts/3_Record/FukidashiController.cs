﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FukidashiController : MonoBehaviour
{
    public Transform cameraTransform;

    public GameObject omedeto;
    public GameObject tanyobi;
    public GameObject nyugaku;
    public GameObject sotsugyo;
    public GameObject kekkon;
    public GameObject arigato;
    public GameObject suki;
    public GameObject gomen;
    public GameObject nenmatsu;
    public GameObject shinnen;
    public GameObject hikkoshi;

    public void ShowMessage(bool Enable)
    {
        omedeto.SetActive(false);
        tanyobi.SetActive(false);
        nyugaku.SetActive(false);
        sotsugyo.SetActive(false);
        kekkon.SetActive(false);
        arigato.SetActive(false);
        suki.SetActive(false);
        gomen.SetActive(false);
        nenmatsu.SetActive(false);
        shinnen.SetActive(false);
        hikkoshi.SetActive(false);
        if (!Enable) return;
        switch (GameSettingManager.Instance.Selected)
        {
            case Config.OMEDETO :
                omedeto.SetActive(true);
                break;
            case Config.TANYOBI:
                tanyobi.SetActive(true);
                break;
            case Config.NYUGAKU:
                nyugaku.SetActive(true);
                break;
            case Config.SOTSUGYO:
                sotsugyo.SetActive(true);
                break;
            case Config.KEKKON :
                kekkon.SetActive(true);
                break;
            case Config.ARIGATO:
                arigato.SetActive(true);
                break;
            case Config.SUKI:
                suki.SetActive(true);
                break;
            case Config.GOMEN:
                gomen.SetActive(true);
                break;
            case Config.NENMATSU:
                nenmatsu.SetActive(true);
                break;
            case Config.SHINNEN:
                shinnen.SetActive(true);
                break;
            case Config.HIKKOSHI:
                hikkoshi.SetActive(true);
                break;
            default:
                break;
        }
    }

    void Update()
    {
        if (cameraTransform != null) transform.LookAt(cameraTransform);
    }
}
