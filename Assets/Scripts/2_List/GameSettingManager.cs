﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettingManager : MonoBehaviour
{
    public static GameSettingManager Instance;
	public GameObject[] Characters;
	public int Selected;
	private float CurrentVolme;
	public AudioClip[] clips;
	private AudioSource NowBgm;
	public static MessageItem[] MessagesList;
	public Sprite[] MessageThumb;
	public string[] MessageTitle;

	void Start()
	{
		if (Instance == null)
		{
			Instance = this;
			Selected = 1;
			NowBgm = GetComponent<AudioSource>();
			InitMessagesList();
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Destroy(this.gameObject);
		}
		//playAudio();
	}

	void InitMessagesList()
	{
		MessagesList = new MessageItem[11];
		for (int i = 0; i< MessagesList.Length; i++)
        {
			MessagesList[i] = new MessageItem(MessageTitle[i], MessageThumb[i], i + 1);
		}
	}

	public void playAudio()
    {
		Debug.LogError("***** " + clips.Length + "   +   " + Selected);
		NowBgm.clip = clips[Selected - 1];
		NowBgm.Play();
		StartCoroutine("FeedInBGM");
	}

	public void stopAudio()
	{
		StartCoroutine("FeedOutBGM");
	}

	public float getAudioLength()
    {
		return clips[Selected - 1].length;
    }

	IEnumerator FeedInBGM()
	{
		float NowfeedTime = 0;
		StopCoroutine("FeedOutBGM");
		while (CurrentVolme < 1)
		{
			NowfeedTime++;
			CurrentVolme = Mathf.Lerp(CurrentVolme, 1, 0.5f);
			yield return null;
			if (NowfeedTime == 20)
			{
				break;
			}
		}
		CurrentVolme = 1;
	}

	IEnumerator FeedOutBGM()
	{
		float NowfeedTime = 0;
		StopCoroutine("FeedInBGM");
		while (CurrentVolme > 0.01)
		{
			NowfeedTime++;
			CurrentVolme = Mathf.Lerp(CurrentVolme, 0, 0.5f);
			yield return null;
			if (NowfeedTime == 20)
			{
				break;
			}
		}
		CurrentVolme = 0;
		NowBgm.Stop();
	}

}

public class MessageItem
{
	public string message = "";
	public Sprite image;
	public int animation = 0;

	public MessageItem(string message, Sprite image, int animation)
	{
		this.animation = animation;
		this.message = message;
		this.image = image;
	}
}