﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;
//using UnityEngine.SceneManagement;

//public class SwipeController : MonoBehaviour
//{
//    public Camera mainCamera;
//    public GameObject CharList;
//    public RectTransform BackGround;
//    RectTransform panel;
//    int select = 1;
//    bool isRunning = false;
//    public float speed = 0.3f;

//    void Start()
//    {
//        Screen.orientation = ScreenOrientation.Portrait;
//        panel = gameObject.GetComponent<RectTransform>();
//        if (GameSettingManager.Instance != null)
//        {
//            GameSettingManager.Instance.stopAudio();
//            GameSettingManager.Instance.Characters = new GameObject[CharList.transform.childCount];
//            for (int i = 0; i < CharList.transform.childCount; i++)
//            {
//                GameObject chara = CharList.transform.GetChild(i).gameObject;
//                GameSettingManager.Instance.Characters[i] = chara;
//            }
//        }
//        select = 1;
//        CharList.GetComponent<Animator>().SetInteger("selected", select);
//    }

//    void Update()
//    {
//#if UNITY_EDITOR
//        Animator animator = GameSettingManager.Instance.Characters[select - 1].transform.GetChild(3).gameObject.GetComponent<Animator>();
//        if (isRunning)
//        {
//            Vector3 delta = new Vector3(2 * (select - 1), 0, 0) - CharList.transform.position;
//            if ((delta.x < 0 && delta.x > -0.3) || (delta.x > 0 && delta.x < 0.3))
//            {
//                CharList.transform.position = new Vector3(2 * (select - 1), 0, 0);
//                BackGround.anchoredPosition = new Vector2(2 * (select - 1) * -500, 0);
//                isRunning = false;
//            }
//            else
//            {
//                CharList.transform.position += Time.deltaTime * speed * delta;
//                BackGround.anchoredPosition += Time.deltaTime* speed * -500 * new Vector2(delta.x, delta.y);
//            }
//        }
//        else if (Input.GetKeyDown(KeyCode.D))
//        {
//            isRunning = true;
//            select++;
//            if (select > 4) select = 1;
//            //CharList.GetComponent<Animator>().SetInteger("selected", select);
//        }
//        else if (Input.GetKeyDown(KeyCode.A))
//        {
//            isRunning = true;
//            select--;
//            if (select < 1) select = 4;
//            //CharList.GetComponent<Animator>().SetInteger("selected", select);
//        }
//        else if (Input.GetKey(KeyCode.Space))
//        {
//            animator.SetTrigger("Win");
//            //StartCoroutine(runAni());

//        }
//        else if (Input.GetKey(KeyCode.G))
//        {
//            animator.SetTrigger("Idle");
//            //StartCoroutine(runAni());

//        }
//#endif

//        if (isRunning)
//        {
//            Vector3 delta = new Vector3(2 * (select - 1), 0, 0) - CharList.transform.position;
//            if ((delta.x < 0 && delta.x > -0.3) || (delta.x > 0 && delta.x < 0.3))
//            {
//                CharList.transform.position = new Vector3(2 * (select - 1), 0, 0);
//                BackGround.anchoredPosition = new Vector2(2 * (select - 1) * -500, 0);
//                isRunning = false;
//            }
//            else
//            {
//                CharList.transform.position += Time.deltaTime * speed * delta;
//                BackGround.anchoredPosition += Time.deltaTime* speed * -500 * new Vector2(delta.x, delta.y);
//            }
//        }
//        else foreach (Touch touch in Input.touches)
//            {
//                //Detects swipe after finger is released
//                if (touch.phase == TouchPhase.Ended)
//                {
//                    Debug.Log("End touch +++++++++++ " + touch.deltaPosition.x);
//                    if (touch.deltaPosition.x < -10)
//                    {
//                        isRunning = true;
//                        select++;
//                        if (select > 4) select = 1;
//                        //CharList.GetComponent<Animator>().SetInteger("selected", select);
//                        Debug.Log("Change +++++++++++ " + select);
//                    }
//                    else if (touch.deltaPosition.x > 10)
//                    {
//                        isRunning = true;
//                        select--;
//                        if (select < 1) select = 4;
//                        //CharList.GetComponent<Animator>().SetInteger("selected", select);
//                        Debug.Log("Change +++++++++++ " + select);
//                    }
//                    else
//                    {
//                        Debug.Log("screenPos " + IsTap(touch.position));
//                        if (IsTap(touch.position))
//                        {
//                            StartCoroutine(RunAni());
//                            Debug.Log("Selected touch +++++++++++ ");
//                        }
//                    }
//                }
//            }
//    }

//    public void ClickButton()
//    {
//        StartCoroutine(RunAni());
//    }

//    bool IsTap(Vector2 tapPos)
//    {
//        Vector2 screenPos = Camera.main.WorldToScreenPoint(GameSettingManager.Instance.Characters[select - 1].transform.position);
//        Debug.Log("screenPos " + screenPos + " - " + tapPos);
//        float deltaPosX = Screen.width / 4;
//        float deltaPosY = Screen.height / 4;
//        if ((tapPos.x - screenPos.x < deltaPosX) && (tapPos.x - screenPos.x > -1 * deltaPosX) && (tapPos.y - screenPos.y < deltaPosY) && (tapPos.y - screenPos.y > -1 * deltaPosY))
//        {
//            return true;
//        }
//        else return false;
//    }

//    IEnumerator RunAni()
//    {
//        isRunning = true;
//        GameSettingManager.Instance.Selected = select;
//        GameSettingManager.Instance.Characters[select - 1].transform.GetChild(0).gameObject.SetActive(false);
//        Animator animator = GameSettingManager.Instance.Characters[select - 1].transform.GetChild(3).gameObject.GetComponent<Animator>();
//        animator.SetTrigger("Move");
//        yield return new WaitForSeconds(1.5f);
//        SceneManager.LoadScene("SampleScene");
//        isRunning = false;
//        Debug.Log("Open new scene +++++++++++ ");
//    }
//}
