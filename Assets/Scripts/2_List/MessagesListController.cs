﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MessagesListController : MonoBehaviour
{
    [SerializeField] private GameObject ListView;
    [SerializeField] private GameObject GridContent;
    [SerializeField] private GameObject ContentItem;

    // Start is called before the first frame update
    void Start()
    {
        InitListItems();
        float rate = (float)Screen.width / (float)Screen.height;
        if(rate > 0.6f)
        {
            GridLayoutGroup grid = GridContent.GetComponent<GridLayoutGroup>();
            grid.cellSize = new Vector2(280, 360);
            grid.spacing = new Vector2(50, 40);
            GridContent.GetComponent<ContentSizeFitter>().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        }
    }

    void InitListItems()
    {
        MessageItem[] list = GameSettingManager.MessagesList;
        for(int i = 0; i < list.Length; i++)
        {
            GameObject obj = Instantiate(ContentItem);
            obj.transform.SetParent(GridContent.transform);
            MessageItemController itemController = obj.GetComponent<MessageItemController>();
            itemController.Init(this, i);
        }
    }

    public void SelectMessage(int id)
    {
        GameSettingManager.Instance.Selected = id;
        SceneManager.LoadScene(Config.name_RECORD);
    }

    public void ProfileTap()
    {
        SceneManager.LoadSceneAsync(Config.name_PROFILE, LoadSceneMode.Additive);
    }

    public void TutorialTap()
    {
        SceneManager.LoadSceneAsync(Config.name_TUTORIAL, LoadSceneMode.Additive);
    }
}
