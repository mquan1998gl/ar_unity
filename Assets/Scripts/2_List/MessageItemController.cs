﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageItemController : MonoBehaviour
{

    [SerializeField] private Image thumb;
    [SerializeField] private Text messages;
    [SerializeField] private Button button;

    public void Init(MessagesListController controller, int id)
    {
        messages.text = GameSettingManager.MessagesList[id].message;
        thumb.sprite = GameSettingManager.MessagesList[id].image;
        button.onClick.AddListener(delegate {
            controller.SelectMessage(GameSettingManager.MessagesList[id].animation);
        });
    }
}
