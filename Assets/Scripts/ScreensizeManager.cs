﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreensizeManager : MonoBehaviour
{

    void Update()
    {
        if ((float)Screen.safeArea.width / (float)Screen.safeArea.height > 1170f/ 2532f)
        {
            GetComponent<CanvasScaler>().matchWidthOrHeight = 0;
        }
        else
        {
            GetComponent<CanvasScaler>().matchWidthOrHeight = 1;
        }

    }

}
