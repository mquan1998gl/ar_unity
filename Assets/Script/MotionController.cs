﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionController : MonoBehaviour
{
    public bool IsSub;
    public MotionController MainController;
    public int CurrentAnime;
    public Vector3 Pos;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //this.gameObject.transform.position = new Vector3(1, 1, 1);
        if (IsSub)
        {
            this.GetComponent<Animator>().SetInteger("Action", MainController.CurrentAnime);
        }
        //this.gameObject.transform.position = new Vector3(Pos.x * this.gameObject.transform.localScale.x, Pos.y * this.gameObject.transform.localScale.x, Pos.z * this.gameObject.transform.localScale.x);
    }

    public void SetAnimetion(int Number)
    {
        CurrentAnime = Number;
        this.GetComponent<Animator>().SetInteger("Action", Number);
    }



}
