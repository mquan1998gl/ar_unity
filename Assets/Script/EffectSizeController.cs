﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectSizeController : MonoBehaviour
{
    public GameObject Main;
    public float size0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.localScale = new Vector3(size0 * Main.transform.localScale.x, size0 * Main.transform.localScale.x, size0 * Main.transform.localScale.x);
    }
}
